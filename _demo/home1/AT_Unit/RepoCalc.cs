﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit
{
    public class RepoCalc
    {
        private IRepo repo;

        public RepoCalc(IRepo repo)
        {
            this.repo = repo;
        }

        public int NextSum()
        {
            int sum = 0;
            repo.NextInts().ForEach(a => sum += a);
            return sum;
        }
    }
}
